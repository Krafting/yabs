<?php
include('creds.php');
// Connexion
try {
    $connexion = new PDO('mysql:host='.$host.';dbname='.$db.'; charset=utf8', $user, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
} catch (PDOExeption $e) {
    echo 'Error';
}

# Regex pas utile, elle est aussi dans le HTACCESS, mais bon, on sais jamais!
if(preg_match_all("#[A-z0-9]+#", $_GET['bin'], $result)) {
    $select = $connexion->prepare('SELECT COUNT(*), content FROM bins WHERE shorturl=:shorturl');
    $select->execute(array('shorturl' => $_GET['bin']));
    $infos = $select->fetch();

    if($infos[0] == 1) {
        $content = $infos['content'];
        header("Content-Type: text/plain; charset=utf-8");
    } else {
        header('Location: index.php?notfound=url');
        exit();
    }
} else {
    header('Location: index.php?notfound=regex');
    exit();
}


?>
<?php echo $content; ?>