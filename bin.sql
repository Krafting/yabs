CREATE TABLE `bins` (
 `idBin` int(11) NOT NULL AUTO_INCREMENT,
 `content` text NOT NULL,
 `shorturl` text NOT NULL,
 `date` text NOT NULL,
 PRIMARY KEY (`idBin`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1