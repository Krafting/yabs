<?php
$status = true;
include('creds.php');
$date = date('d/m/Y_H:i:s');
// Connexion
try {
    $connexion = new PDO('mysql:host='.$host.';dbname='.$db.'; charset=utf8', $user, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
} catch (PDOExeption $e) {
    echo 'Error';
}

function getNumURL($shorten) {
    global $connexion;
    $select = $connexion->prepare('SELECT COUNT(*) FROM urls WHERE shorturl=:shorturl');
    $select->execute(array('shorturl' => $shorten));

    $count = $select->fetch();
    return $count[0];
}


function generate_random_letters($length) {
    $random = '';
    for ($i = 0; $i < $length; $i++) {
        $random .= rand(0, 1) ? rand(0, 9) : chr(rand(ord('a'), ord('z')));
    }
    return $random;
}

if($status == true) {
    if(isset($_POST['sendBin']) AND !empty($_POST['bin'])) {
        do {
            $shorturl = generate_random_letters(12);
        } while (getNumURL($shorturl) > 0);
        $content = $_POST['bin'];
        $addToBdd = $connexion->prepare('INSERT INTO bins (content, shorturl, date) VALUES (:content, :shorturl, :date)');
        $addToBdd->execute(array(
            ':content' => $content,
            ':shorturl' => $shorturl,
            ':date' => $date
        ));
        header('Location: index.php?link='.$shorturl);
        exit();
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="css.css">
    <link rel="stylesheet" href="//cdn.krafting.net/fonts/jura.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>KRAFTING.net - YABS (Yet Another Bin Service)</title>
</head>
<body>
    <header>
        <h1><a href="https://krafting.net">KRAFTING.net</a></h1>
        <h2><a href="./">YABS - Yet Another Bin Service</a></h2>
    </header>
    <div class="underglow"></div>
    <div class="content">
        <div class="yauss">
            <p>YABS is a free service offered by Krafting. It allows you to simply share text or code.</p>
            <small>Note: Nothing is encrypted.</small>
            <?php if(isset($_GET['link'])) { echo '<label class="success">Bin created! Your Link: <a href="'.$urlSite.$_GET['link'].'">'.$urlSite.$_GET['link'].'</a><a href="#qrcode" class="qrlink">[QR Code]</a></label>'; }?>
            <?php if(isset($_GET['notfound'])) { echo '<label class="error">This URL is not in the database. Sorry :(</label>'; }?>
            <?php if(!$status) { echo '<label class="error">YABS has been disabled by Krafting. You cannot use this service for the moment.</label>'; }?>

            <form method="POST">
                <textarea rows="15" placeholder="Your content goes here. Code, Text, Passwords, etc.." name="bin"></textarea>
                <input type="submit" name="sendBin" value="Get Bin!">
            </form>
            <div id="qrcode"></div>
        </div>
    </div>
    
    <div class="underglow"></div>
    <footer>
        <a href="https://www.krafting.net/contact.php">Contact</a> 2021 - 2022 - bin.krafting.net <a href="https://www.krafting.net/terms.php" title="Terms & Privacy">Terms & Privacy</a>
	</footer>
    <script src="qrcode.min.js"></script>
    <script src="js.js"></script>

</body>
</html>