<?php
include('creds.php');
// Connexion
try {
    $connexion = new PDO('mysql:host='.$host.';dbname='.$db.'; charset=utf8', $user, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
} catch (PDOExeption $e) {
    echo 'Error';
}

# Regex pas utile, elle est aussi dans le HTACCESS, mais bon, on sais jamais!
if(preg_match_all("#[A-z0-9]+#", $_GET['bin'], $result)) {
    $select = $connexion->prepare('SELECT COUNT(*), content FROM bins WHERE shorturl=:shorturl');
    $select->execute(array('shorturl' => $_GET['bin']));
    $infos = $select->fetch();

    if($infos[0] == 1) {
        $content = htmlspecialchars($infos['content']);
    } else {
        header('Location: index.php?notfound=url');
        exit();
    }
} else {
    header('Location: index.php?notfound=regex');
    exit();
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="css.css">
    <link rel="stylesheet" href="//cdn.krafting.net/fonts/jura.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>KRAFTING.net - YABS (Yet Another Bin Service)</title>
</head>
<body>
    <header>
        <h1><a href="https://bin.krafting.net">KRAFTING.net</a></h1>
        <h2><a href="./">YABS - Yet Another Bin Service</a></h2>
    </header>
    <div class="underglow"></div>
    <div class="content">
        <div class="yauss">
            <p>YABS is a free service offered by Krafting. It allows you to simply share text or code.</p>
            <textarea rows="15" disabled placeholder="Your content goes here. Code, Text, Passwords, etc.." type="text" name="bin" pattern="http(s)?://(www.)?(.)+"><?php echo $content; ?></textarea>
            <h1>Raw Data <span><a href="./raw/<?php echo $_GET['bin']; ?>">Get Raw Link</a></span></h1>
            <textarea rows="15" placeholder="Your content goes here. Code, Text, Passwords, etc.." type="text" name="bin" pattern="http(s)?://(www.)?(.)+"><?php echo $content; ?></textarea>
        </div>
    </div>
    
    <div class="underglow"></div>
    <footer>
        <a href="https://www.krafting.net/contact.php">Contact</a> 2021 - 2022 - bin.krafting.net <a href="https://www.krafting.net/terms.php" title="Terms & Privacy">Terms & Privacy</a>
	</footer>
</body>
</html>